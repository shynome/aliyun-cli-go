package common

// Parameters 阿里云都会用到的通用参数
type Parameters struct {
	Format           string `json:"Format"`
	Version          string `json:"Version"`
	SignatureVersion string `json:"SignatureVersion"`
	SignatureMethod  string `json:"SignatureMethod"`
	SignatureNonce   string `json:"SignatureNonce"`
	TimeStamp        string `json:"TimeStamp"`
}
