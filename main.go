package main

import (
	"log"
	"os"

	"aliyun-cli/cmd"
)

func main() {
	
	err := cmd.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}
