package cmd

import (
	"aliyun-cli/cmd/cdn"
	"aliyun-cli/common"
	"github.com/joho/godotenv"
	"strings"

	"github.com/urfave/cli"
)

// MakeDroneEnvKeys 生成使用 Drone 进行执行的时候可能用到参数
func MakeDroneEnvKeys(key string) (keys string) {
	var prefixStrs = []string{"PLUGIN_", ""}
	for _, prefix := range prefixStrs {
		envkey := prefix + key
		keys = keys + "," +strings.ToUpper(envkey)
		keys = keys + "," + envkey
	}
	keys = keys[1:]
	return
}

// App of aiyun cmd
var App = &cli.App{
	Name:    "aliyun",
	Version: "0.0.0",
	Usage:   "aliyun cli tools",
	Commands: []cli.Command{
		cdn.Command,
	},
	Flags: []cli.Flag{
		cli.StringFlag{
			Name:   "AccessKeyId, access_key_id",
			EnvVar: MakeDroneEnvKeys("AccessKeyId") + MakeDroneEnvKeys("access_key_id"),
			Destination: &common.AccessKeyID,
		},
		cli.StringFlag{
			Name: "AccessKeySecret, access_key_secret",
			EnvVar: MakeDroneEnvKeys("AccessKeySecret") + MakeDroneEnvKeys("access_key_secret"),
			Destination: &common.AccessKeySecret,
		},
	},
}

// Run of cmd App
func Run(args []string) (err error) {
	err = godotenv.Load()
	if err != nil {
		return err
	}
	err = App.Run(args)
	return 
}
