package flush

import (
	"fmt"
	"os"

	"github.com/urfave/cli"
)

const tip = `
刷新 cdn 的触发方式:

  1. 提交信息中包含 [cdn refresh] 或 [flush cdn] ;
  2. 打 tag 并提交到 git 仓库

(ps: cdn 刷新大概需要 1, 2 分钟才会生效)
`

// Command of cdn flush
var Command = cli.Command{
	Name:    "flush",
	Aliases: []string{"RefreshObjectCaches"},
	Usage:   "[origin]",
	Flags:   []cli.Flag{},
	Action: func(c *cli.Context) (err error) {
		if !(os.Getenv("DRONE_BUILD_EVENT") == "tag" || checkCommitMessage(os.Getenv("DRONE_COMMIT_MESSAGE"))) {
			fmt.Println(tip)
			return
		}
		return
	},
}
