package cdn

import (
	"github.com/urfave/cli"
	"aliyun-cli/cmd/cdn/flush"
)

// Command of cdn
var Command = cli.Command{
	Name: "cdn",
	Subcommands: []cli.Command{
		flush.Command,
	},
}
